﻿recipeNs.recipeApp.controller('demoController',['$scope', '$timeout',
    function ($scope, $timeout) {
        $scope.greeting = 'Hello';
        var promise = $timeout(function () {
            $scope.greeted = 'World';
            console.log('timeout');
        }, 1000);
        
        $scope.action = function (msg) {
            alert(msg);
        };
        $scope.cancel = function () {
            $timeout.cancel(promise);
        };
    }]);