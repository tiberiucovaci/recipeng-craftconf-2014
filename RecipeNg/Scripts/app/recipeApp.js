﻿var recipeNs = recipeNs || {};

recipeNs.recipeApp = angular.module('recipeApp', ['ngResource', 'ngRoute'],
    function ($routeProvider) {
        $routeProvider.when('/',
            {
                templateUrl: 'Templates/home.html'
            });
        $routeProvider.when('/recipe/:id', {
            templateUrl: 'Templates/recipeDetails.html',
            controller: 'recipeController',
            resolve: {
                recipe: ['$route', 'resourceRecipeDataService', function ($route, dataService) {
                    return dataService.getRecipe($route.current.pathParams.id).$promise;
                }]
            }
        });
    });
