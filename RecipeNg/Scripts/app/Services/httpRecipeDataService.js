﻿recipeNs.recipeApp.service('httpRecipeDataService', ['$http',
    function ($http) {

        this.getRecipe = function (id, successFn) {
            $http.get('/api/Recipes/' + id).success(function (data, status, headers, config) {
                successFn(data);
            }).
            error(function (data, status, headers, config) {
                console.log(status);
            });
        };
        this.getRecipies = function (successFn) {
            $http.get('/api/Recipes').success(function (data, status, headers, config) {
                successFn(data);
            }).
            error(function (data, status, headers, config) {
                console.log(status);
            });
        };

        return this;
    }]); 