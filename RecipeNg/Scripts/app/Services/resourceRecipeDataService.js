﻿recipeNs.recipeApp.service('resourceRecipeDataService', ['$resource',
    function ($resource) {
        var resource = $resource('/api/Recipes/:id', { id: '@id' });

        this.getRecipe = function (id) {
            return resource.get({id : id});
        };
        this.getRecipies = function () {
            return resource.query();
        };

        return this;
}]);