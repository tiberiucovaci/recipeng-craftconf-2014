﻿recipeNs.recipeApp.service('httpRecipeDataServiceWithq', ['$http', '$q',
    function ($http, $q) {

        this.getRecipe = function (id) {
            var deffered = $q.defer();
            $http.get('/api/Recipes/' + id).success(function (data, status, headers, config) {
                deffered.resolve(data);
            }).
            error(function (data, status, headers, config) {
                deffered.reject(status);
            });
            return deffered.promise;
        };

        this.getRecipies = function (successFn) {
            var deffered = $q.defer();
            $http.get('/api/Recipes').success(function (data, status, headers, config) {
                    deffered.resolve(data);
                }).
            error(function (data, status, headers, config) {
                deffered.reject(status);
            });
            return deffered.promise;
        };

        return this;
    }]);