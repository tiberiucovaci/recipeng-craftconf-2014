﻿recipeNs.recipeApp.directive('myNavigation', function () {
    return {
        restrict : 'AE',
        templateUrl: 'templates/directives/myNavigation.html',
        replace : true
    }
});